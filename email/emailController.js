const constant          = require('../constant');
const nodemailer        = require('nodemailer');
const moment            = require('moment');


/*
 * @description Send Email
 * @param {object} req
 * @param {object} res
 * @returns {json}
 */
 exports.sendMail = (req, res, next) => {
		//const {email, fname, lname} = req.body;
		let email = "vanya.jain@jktech.com";
		let fname = "vanya";
		let lname = "jain";

		var emailSubject = '';
		
		var transporter = nodemailer.createTransport({
			host: constant.mail.host, // hostname
			secureConnection: false, // TLS requires secureConnection to be false
			port: constant.mail.port, // port for secure SMTP
			tls: {
			ciphers:'SSLv3'
			},
			auth: {
				user: constant.mail.from,
				pass: constant.mail.pass
			}
		});

		let firstName = fname.charAt(0).toUpperCase() + fname.slice(1);
		var reqHtml = `Hey ${firstName}, <br/><br/> Thanks for signing up. To get started, click the below link to confirm your account.`;
			emailSubject = constant.mail.confirmEmailSubject;
		
			
		var opts = {
				from: constant.mail.from,
				to: email,
				subject: emailSubject,
				html: reqHtml
				/*attachments: [{   // filename and content type is derived from path
					path: transcriptFilePath
				}]*/
			}
		
		transporter.sendMail(opts, function(err, info){
			if(err){
				console.error(err);
				res.status(400);
				res.send({"message":"some error occured"});
			} else{
				console.log('Mail sent', info);
				res.status(200);
				res.send({"message":"mail sent"});
				
			}
		});
    }
