const constant = require(__basePath + 'app/config/constant');
const router   = require('express').Router({
    caseSensitive: true,
    strict       : true
});

const emailController  = require(constant.path.controller + 'email/emailController');

//router.post('/sendEmail', emailController.sendEmail);

module.exports = {
    router: router
};
