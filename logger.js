const constant          = require('./constant');
const winston           = require('winston');
const moment            = require('moment');
const nodeEnv           = process.env.NODE_ENV || 'development';
var csvFilePath         = constant.path.log + nodeEnv + '-' + "csvLog" + '-' + moment().format('YYYY_MM_DD') + '.csv';


const loggerCSV = new (winston.Logger)({
    emitErrs  : false,
    transports: [
        new (winston.transports.File)(
            {      
            filename       : csvFilePath,
            json           : false,
            formatter      : function (args) {
                                return args.message;
                            }
        })
    ]
});

const logger = new (winston.Logger)({
    emitErrs  : false,
    transports: [
        /*new winston.transports.Console({
            level          : constant.logging.consoleLevel,
            label          : constant.logging.label,
            handleException: true,
            json           : false,
            colorize       : true,
            formatter      : function (options) {
                return "[" + moment().format('YYYY-MM-DD hh:mm:ss') + "] " +
                    options.label + "." + options.level.toUpperCase() + ": " + (options.message ?
                                                                                options.message :
                                                                                '') +
                    (options.meta && Object.keys(options.meta).length ?
                     '\n' + JSON.stringify(options.meta, null, 4) :
                     '');
            }
        }),*/

        new (winston.transports.File)(
            {      
            level          : constant.logging.fileLevel,
            label          : constant.logging.label,
            name           : 'log_file',
            filename       : constant.path.log + nodeEnv + '-' + moment().format('YYYY_MM_DD') + '.log',
            handleException: true,
            json           : false,
            maxSize        : 52428800,
            maxFiles       : 10,
            prettyPrint    : true,
            formatter      : function (options) {
                return "[" + moment().format('YYYY-MM-DD HH:mm:ss') + "] " +
                    options.label + "." + options.level.toUpperCase() + ": " + (options.message ?
                                                                                options.message :
                                                                                '') +
                    (options.meta && Object.keys(options.meta).length ?
                     '\n' + JSON.stringify(options.meta, null, 4) :
                     '');
            }
        })
    ]
});

var writeLogError = function(name, errMsg, errStatusCode, errStack){
    var error_json = {
        functionName: name,
        message: errMsg,
        statusCode: errStatusCode,
        stack: errStack
    }
    logger.error(error_json);
}

var writeLogInfo = function(name, msg, apiResponse){
    var info_json = {
        functionName: name,
        message: msg,
        info: apiResponse
    }
    logger.info(info_json);
}

var writeCSVInfo = function(msg){
    loggerCSV.info(msg);
}


 /** Return Logger instances */
 module.exports = {
    logger          : logger,
    writeLogError   : writeLogError,
    writeLogInfo    : writeLogInfo,
    writeCSVInfo    : writeCSVInfo
 };