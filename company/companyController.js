
const request = require('request');
const moment = require('moment');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const constant          = require('../constant');
const jwt = require('jsonwebtoken');
var CryptoJS = require("crypto-js");
const loggerObj = require("../logger");

exports.createCompany = async (req, res, next) => {
    let { email, companyName } = req.body;
    try {
         console.log("company name is", companyName);
         console.log("email is:", email);
         let JWTToken = jwt.sign({ email: email, companyName: companyName }, constant.JWTDetails.key, { expiresIn: "1d" });
        var ciphertext = CryptoJS.AES.encrypt(JWTToken, 'secret key 123').toString();
        req.jwtToken = ciphertext;
        var token = "U2FsdGVkX19oXSsqCBLLGIO/bsbEr9wh4aoxLBqUC4S/OKqqwZ2QGC+nMf17T+ufsgfAq9QyUvjxG3YKXKh0gQaIvkFxcwEryGX+B9ojVdXL52NNOfT8eEMpJ9xSCjdu9xm/UUwptiplxbH9Ooh4rWlIssZDGPI4+kx+x77N14cgOuHxb3AJ/Cab6eZj+nfVhzuS+RfADX0gIzI1JnAuWFsVBJZvC+t1ymLdaVfY9z0R7zJMtkGV8ejYxFO8PdKgqCBJnUh9v8iJQJtNHHiC/f20/ML96t/gHWmE1Qs3R4Y=";

        var bytes = CryptoJS.AES.decrypt(token, 'secret key 123');
        var originalText = bytes.toString(CryptoJS.enc.Utf8);
        jwt.verify(originalText, constant.JWTDetails.key, function (error, decoded) {
            if (error) {
                
                res.send({
                    error: (error.message) ? error.message : error.message
                });
            } else {
                
                next();
            }
        });
           
        
    } catch (error) {
        console.log("error is:" + error);
        var errorMessage = (error.message != undefined) ? error.message : '';
        var errorStatusCode = (error.statusCode != undefined) ? error.statusCode : '';
        var errorStack = (error.stack != undefined) ? error.stack : '';
       loggerObj.writeLogError('createCompany', errorMessage, errorStatusCode, errorStack);
        res.send({
            error: (error.message) ? error.message : error.message
        });
    }
}
exports.sendMsg = async (req, res, next) => {
    try{
        res.status(200);
        //loggerObj.writeLogError('createCompany', errorMessage, errorStatusCode, errorStack);
         return res.send({
             message: 'Company Created Successfully'
         });
    }catch(error){
        
    }
}