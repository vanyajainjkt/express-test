const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
var _ = require('lodash');

function isValidEmail(email) {
    var pattern = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
    return new RegExp(pattern).test(email);
}

exports.createCompanyValidation = async(req, res, next) => {
    let errors = [];

    let { email, companyName } = req.body;

    console.log("company name is", companyName);

    if (_.isEmpty(companyName)) {
        errors.push({ key: "companyName", message: "Company name is mandatory." });
    }
    if (_.isEmpty(email)) {
        errors.push({ key: "email", message: "Email is mandatory." });
    }
    if (!isValidEmail(email)) {
        errors.push({ key: "email", message: "Email is not valid." });
    }
    
    if (errors && errors.length) {
        res.status(400);
        return res.send({
            message: 'Validation error.',
            error: errors
        });
    } else {
        next();
    }
}
