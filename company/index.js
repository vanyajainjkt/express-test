const constant = require(__basePath + 'app/config/constant');
const router = require('express').Router({
    caseSensitive: true,
    strict: true
});

const companyController = require(constant.path.controller + 'company/companyController');
const companyValidation = require(constant.path.controller + 'company/validation');

router.post('/signup',
    companyValidation.createCompanyValidation,
    companyController.createCompany
);


module.exports = {
    router: router
};