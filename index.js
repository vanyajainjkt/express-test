const restify = require('restify');
const companyController = require('./company/companyController');
const companyValidation = require('./company/validation');

const emailController = require('./email/emailController');


const server = restify.createServer();
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());



/*server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());*/
server.listen(process.env.port || process.env.PORT || 3978, () => {
    console.log(`\n${ server.name } listening to ${ server.url }`);
});

server.get('/:id', (req, res) => {
    console.log("id is", req.params.id);
    if(req.params.id){
        res.send({"message":"Server is running with restify", "id": req.params.id});
    } else {
        res.send({"message":"Server is running with restify"});
    }
   
})

/*server.post('/', (req, res) => {
	console.log(req.body);
    res.send("Server is running with restify post method");
})*/


server.post('/',
    companyValidation.createCompanyValidation,
    companyController.createCompany,
    companyController.sendMsg,
);


server.post('/email',
    emailController.sendMail
);